#include "minesweeperbutton.h"
#include <QMouseEvent>
#include <QDebug>

//Detect Left click
MineSweeperButton::MineSweeperButton(QWidget *parent) : QPushButton(parent)
{
}

MineSweeperButton::MineSweeperButton(QString blah) : QPushButton(blah)
{
}

//Enables Right clicking
void MineSweeperButton::mousePressEvent(QMouseEvent *qMEvent)
{
    //If we right click
    if ( qMEvent->button() == Qt::RightButton )
    {
        emit rightButtonClicked();      //emit rightButtonSignal
    }

    //Do default behavior otherwise
    QPushButton::mousePressEvent(qMEvent);
}

MineSweeperButton::MineSweeperButton(int row,int column,QWidget *parent) : QPushButton(parent){
    this->row = row;
    this->column = column;
    this->flaged = false;
}

int MineSweeperButton::getRow() const{
    return row;
}
int MineSweeperButton::getColumn() const{
    return column;
}
bool MineSweeperButton::isFlaged() const{
    return flaged;
}
void MineSweeperButton::setFlaged(bool flag_state){
    this->flaged = flag_state;
}
