#include <QApplication>

#include "window.h"
#include "board.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    myWindow window;
    window.show();

    return app.exec();
}
