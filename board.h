#ifndef BOARD_H
#define BOARD_H

#include <QVector>

#include <cstdlib>
#include <ctime>

#define XOR_NUM 6

class Board{
    private:
        QVector< QVector<int> > gameBoard;//Vector for the board
        int rows;   //Number of Rows
        int cols;  //Number of columns
        int Nbombs; //Number of Bombs
        bool isInsideTheBoard(int, int); //True if coordinates are inside the board
     public:
        QVector< QVector<int> > getBoard(); //Return the vector of the board
        Board(int Nbombs, int rows, int cols); //First constructor
        Board(QVector< QVector<int> >); //Second constructor
        int calculateNeighborsBombs(int x,int y);
        int getNumberOfBombs(int x,int y);
        int getNbombs() const;
        QVector< QVector<int> > getEncryption();
        QVector< QVector<int> > getDecryption(QVector< QVector<int> >);

};

#endif // BOARD_H
